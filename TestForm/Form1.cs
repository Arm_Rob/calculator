﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestForm.Properties;

namespace TestForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        #region Buttons

        private void Button0_Click(object sender, EventArgs e) => GetNum("0");
        private void Button1_Click(object sender, EventArgs e) => GetNum("1");
        private void Button2_Click(object sender, EventArgs e) => GetNum("2");
        private void Button3_Click(object sender, EventArgs e) => GetNum("3");
        private void Button4_Click(object sender, EventArgs e) => GetNum("4");
        private void Button5_Click(object sender, EventArgs e) => GetNum("5");
        private void Button6_Click(object sender, EventArgs e) => GetNum("6");
        private void Button7_Click(object sender, EventArgs e) => GetNum("7");
        private void Button8_Click(object sender, EventArgs e) => GetNum("8");
        private void Button9_Click(object sender, EventArgs e) => GetNum("9");
        private void Dot_Click(object sender, EventArgs e) => GetNum(".");

        private void Plus_Click(object sender, EventArgs e) => GetAction("+");
        private void Minus_Click(object sender, EventArgs e) => GetAction("-");
        private void Mult_Click(object sender, EventArgs e) => GetAction("*");
        private void Div_Click(object sender, EventArgs e) => GetAction("/");

        #endregion

        private void Equal_Click(object sender, EventArgs e)
        {
            if (num1 == null && num2 == null && act == null)
                return;

            DoAction();

            num1 = null;
            num2 = null;
            act = null;
            isFirst = true;
        }

        bool isFirst = true;
        string act;

        void DoAction()
        {
            double? res = null;
            double n1 = Convert.ToDouble(num1);
            double n2 = Convert.ToDouble(num2);
            switch (act)
            {
                case "+":
                    res = n1+n2;
                    break;
                case "-":
                    res = n1 - n2;
                    break;
                case "*":
                    res = n1 * n2;
                    break;
                case "/":
                    if (n2 == 0)
                    {
                        richTextBox1.Text = "Can not divide by 0";
                        return;
                    }

                    res = n1 / n2;
                    break;
            }
            string last = Draw(num1,num2);
            richTextBox1.Text =  last + "=" + res.ToString();
        }

        string num1;
        string num2;
        private void GetNum(string num)
        {
            if (isFirst)
            {
                num1 += num;
                Draw(num1, null);

            }
            else
            {
                num2 += num;
                Draw(num1, num2);

            }

        }

        void GetAction(string symbol)
        {
            if (num1 == null)
                return;

            isFirst = false;
            act = symbol;
            Draw(num1, num2);
        }

        string Draw(string n1, string n2)
        {
            richTextBox1.Text = n1 +act+ n2;
            return n1 + act + n2;
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            num1 = null;
            num2 = null;
            act = null;
            isFirst = true;
            richTextBox1.Text = null;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            Size = Settings.Default.LastSize;
            Location = Settings.Default.LastLocation;
            WindowState = Settings.Default.LastState;

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.LastState = WindowState;
            if (this.WindowState == FormWindowState.Normal)
            {
                Settings.Default.LastLocation = Location;
                Settings.Default.LastSize = Size;
            }
            else
            {
                Settings.Default.LastLocation = RestoreBounds.Location;
                Settings.Default.LastSize = RestoreBounds.Size;
            }

            Settings.Default.Save();

        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            if (Size.Width < 250 || Size.Height < 400)
            {
                Size = new Size(250, 400);
                Settings.Default.LastSize = new Size(250, 400);

            }
        }
    }
}
